<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'yishop');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F/a,3-Obe`<]W_&45X[[m^Qr1!b^Ia&<i/xU8,4%t>X^KZ:fS}o#tnz3?$R$JemY');
define('SECURE_AUTH_KEY',  '{QYFzLU+L=RAeX:X%|aQw_;2LHka lzLun+n3q_>eQLv2is@Q+WF~r?;0U~ZXiSR');
define('LOGGED_IN_KEY',    'OeoE7D^PC<`h:/urTXnhPKT#:EZ;k|FHIZU>eJQ0F(l8i!4wgE:~5XD[twJp_Wj$');
define('NONCE_KEY',        'V@<oN6[dZJ>>]v2131,g(f3H|O;p{@p5td:qNJqO,pN|M;S~Vf/rLB KoI{4&h]>');
define('AUTH_SALT',        '1. QM%3ZhdfHBzYKzZhXcW$-p.~0yHH{J99diur{{kO(_D_}CI9n!BwxLbsdnR?<');
define('SECURE_AUTH_SALT', '25.4J*d|ICWKE>#Ys{Op@8Oq_itb7Z{q&&rPWl+G7N**gKd*$aQ8eH2#RJ<84w&P');
define('LOGGED_IN_SALT',   'E|7u.w-s]8Ja$2.tDdad;dKCl@%X,4ywF;{0r[^W_p1#X}*0BUF06MtPK9g4]z7m');
define('NONCE_SALT',       'Z7c?2Dw#*r;W3/1pORYW^0r(a~MiiV~J^hZRj/F9pA(63!]#[z:,}bI2#)cU&VK*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'l4c_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
