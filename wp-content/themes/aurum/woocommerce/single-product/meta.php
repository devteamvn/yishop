<?php
/**
 * Single Product Meta
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

/* Note: This file has been altered by Laborator */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Danh Mục:', 'Danh Mục:', $cat_count, 'aurum' ) . ' ', '.</span>' ); ?>

	<div id="hotrodathang">
<span>HỖ TRỢ ĐẶT HÀNG</span>
   <div id="hotrodathangdt">
       <img src="http://YiSHOP.vn/wp-content/uploads/2016/06/icon-phone-support.png" style="float: left; margin-right: 10px;">   
       <p>098 145.44.44</p> 
       <p>0914.042.055</p>
   </div>
<p>Điện thoại: 8:30 - 21:30 (Thứ 2 đến CN)</p>
<p>Địa chỉ : Lầu 1 - 90 An Dương Vương, P9,Q5</p>
</div>

<button class="accordion buttontop">Facebook của YiSHOP</button>
<div class="panel">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=1037003799654707";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
 <div class="fb-page" data-href="https://www.facebook.com/YiSHOP.vn/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/YiSHOP.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/YiSHOP.vn/">Yishop.vn</a></blockquote></div>
 </div>


<button class="accordion buttontop">Hướng dẫn chọn Size</button>
<div class="panel">
  <p><img src="http://YiSHOP.vn/wp-content/uploads/2016/06/chon-size-2.jpg" alt="Cách chọn size"></p>
  <p>Kích thước có thể thay đổi 1-3cm do tính chất loại vải có độ co giãn hoặc thiết kế kiểu dáng khác nhau.</p>
</div>

<button class="accordion">Vận chuyển - Giao Hàng</button>
<div class="panel">
  <p><strong>TPHCM :</strong>
<ul>
<li>Miễn phí giao hàng tất cả các Quận nội thành TP.HCM với thời gian giao hàng tối đa từ 3h-5h ngay sau khi nhân viên xác nhận đơn hàng của Quý Khách.</li>
<li>Các Quận ngoại thành YiSHOP phụ thu phí giao hàng mỗi đơn hàng là 30.000đ/1 đơn hàng. Quý khách mua 2 sản phẩm trở lên miễn phí giao hàng</li>
</ul>
<p><strong>CÁC TỈNH THÀNH KHÁC :</strong></p>
<ul>
<li>Miễn phí giao hàng với tất cả các khách hàng chuyển khoản trả trước.</li>
<li>Chuyển COD tới 64 tỉnh thành trong cả nước với cước phí 30.000đ/ 1 sản phẩm.</li>
<li> Miễn phí gửi hàng khi mua từ 2 sản phẩm</li>
<li>Thời gian giao hàng từ 03-05 ngày làm việc kể từ khi đơn hàng được xác nhận thành công</li>
<li>Với những yêu cầu đặc biệt gửi hỏa tốc hoặc gửi xe Quý khách vui lòng liên hệ hotline : 098 145.44.44 - 0914.042.055 (Mọi chi phí vận chuyển theo hình thức này Quý khách tự thanh toán với đối tác vận chuyển)</li>
<li>YiSHOP ký hợp đồng vận chuyển với bưu điện VNPT và Viettel đảm bảo hàng hóa đến tận tay Quý Khách với thời gian ngắn nhất.</li></ul></p>
</div>


<button class="accordion">Chính sách đổi hàng</button>
<div class="panel">
  <p>
<ul>
<li>Hàng đổi phải còn mới và giữ nguyên tình trạng ban đầu, còn nguyên tem, nhãn mác;
<li>Hàng chưa qua sử dụng, chưa giặt và không bị hư hại. Hàng không bị bẩn, không dính các vết mỹ phẩm, không nhiễm mùi như nước hoa, kem cạo râu, chất khử mùi cơ thể hay khói thuốc lá, v.v…;
<li>Hàng bán trong thời gian khuyến mãi, hoặc hàng đã được đổi một lần trước đó sẽ không áp dụng chính sách đổi trả hàng, trừ trường hợp do lỗi kỹ thuật;
<li>Giá trị đổi hàng tính theo giá bán thực tế tại thời điểm mua hàng, nhưng phải bằng hoặc cao hơn sản phẩm muốn đổi;
<li>Nếu quý khách không chọn được sản phẩm nào ưng ý tại thời điêm đổi hàng hoặc sản phẩm mới đổi thấp hơn sản phẩm cũ, YiSHOP sẽ ghi số tiền chênh lệch này thành số tiền đặt cọc của Quý Khách trong lần mua tới (Phiếu đặt cọc có giá trị trong vòng 5 tháng).
</ul>

<strong>Đối với mua hàng online:</strong>
<ul>
<li>Khách hàng liên hệ với bộ phận bán hàng Online để xác nhận đơn hàng muốn đổi và được hướng dẫn đổi hàng (không quá 7 ngày kể từ ngày nhận được sản phẩm);
<li>Hàng chỉ đổi lại khi lỗi do nhà sản xuất, sai màu, sai cỡ như khách hàng yêu cầu;
<li>Khi quý khách muốn đổi lại hàng trong trường hợp không phải do lỗi của YiSHOP, xin vui lòng thanh toán phí vận chuyển.
</ul>
<strong>Đối với mua hàng tại cửa hàng:</strong>
<ul>
<li>Quý khách vui lòng xuất trình hóa đơn mua hàng khi có yêu cầu đổi hàng;
<li>Với mỗi đơn hàng chỉ được đổi tối đa 01 lần (trừ những trường hợp đặc biệt).
</ul>
</p>
</div>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
  }
}
</script>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>