<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

/* Note: This file has been altered by Laborator */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( $order ) : ?>

	<?php if ( $order->has_status( 'failed' ) ) : ?>

		<p><?php _e( 'Thật không may đặt hàng của bạn không thể được xử lý như các ngân hàng có nguồn gốc / thương gia đã từ chối giao dịch của bạn.', 'aurum' ); ?></p>

		<p><?php
			if ( is_user_logged_in() )
				_e( 'Hãy cố gắng mua hàng của bạn một lần nữa hoặc đi đến trang tài khoản của bạn.', 'aurum' );
			else
				_e( 'Hãy cố gắng mua hàng của bạn một lần nữa.', 'aurum' );
		?></p>

		<p>
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Mua', 'aurum' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
			<a href="<?php echo esc_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>" class="button pay"><?php _e( 'Tài khoản', 'aurum' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>

		<p class="alert alert-success def-m"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Xin cảm ơn. Đơn hàng của bạn đã được gửi đi.', 'aurum' ), $order ); ?></p>

		<ul class="order_details order-details-list">
			<li class="order">
				<?php _e( 'Đơn hàng:', 'aurum' ); ?>
				<strong><?php echo $order->get_order_number(); ?></strong>
			</li>
			<li class="date">
				<?php _e( 'Ngày:', 'aurum' ); ?>
				<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
			</li>
			<li class="total">
				<?php _e( 'Tổng:', 'aurum' ); ?>
				<strong><?php echo $order->get_formatted_order_total(); ?></strong>
			</li>
			<?php if ( $order->payment_method_title ) : ?>
			<li class="method">
				<?php _e( 'Phương thức thanh toán:', 'aurum' ); ?>
				<strong><?php echo $order->payment_method_title; ?></strong>
			</li>
			<?php endif; ?>
		</ul>
		<div class="clear"></div>

	<?php endif; ?>

	<div class="bacs-entries">
	<?php do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
	</div>

	<?php do_action( 'woocommerce_thankyou', $order->id ); ?>

<?php else : ?>

	<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Xin cảm ơn. Đơn hàng của bạn đã được gửi đi.', 'aurum' ), null ); ?></p>

<?php endif; ?>