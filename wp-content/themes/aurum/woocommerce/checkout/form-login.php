<?php
/**
 * Checkout login form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

/* Note: This file has been altered by Laborator */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( is_user_logged_in() || 'no' === get_option( 'woocommerce_enable_checkout_login_reminder' ) ) return;

/*
$info_message  = apply_filters( 'woocommerce_checkout_login_message', __( 'Returning customer?', 'aurum' ) );
$info_message .= ' <a href="#" class="showlogin">' . __( 'Click here to login', 'aurum' ) . '</a>';
wc_print_notice( $info_message, 'notice' );
*/

woocommerce_login_form(
	array(
		'message'  => __( 'Nếu bạn đã đi mua sắm với chúng tôi trước đây, vui lòng nhập thông tin của bạn vào các ô bên dưới. Nếu bạn là khách hàng mới, hãy tiến hành thanh toán &amp; giao hàng.', 'aurum' ),
		'redirect' => get_permalink( wc_get_page_id( 'checkout' ) ),
		'hidden'   => true
	)
);
